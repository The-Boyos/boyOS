.code16
.intel_syntax noprefix
.text
.org 0x0

LOAD_SEGMENT = 0x1000

.global main
main:
  jmp short start

bootsector:
  iOEM: .ascii "boyOS   "
  iSectSize: .word 0x200
  iClustSize: .byte 1
  iResSect: .word 1
  iFatCnt: .byte 2
  iRootSize: .word 224
  iTotalSect: .word 2880
  iMeda: .byte 0xF0
  iFatSize: .word 9
  iTrackSect: .word 9
  iHeadCnt: .word 2
  iHiddenSect: .int 0
  iSect32: .int 0
  iBootDrive: .byte 0
  iReserved: .byte 0
  iBootSign: .byte 0x29
  iVolID: .ascii "seri"
  acVolumeLabel: .ascii "MYVOLUME   "
  acFSType: .ascii "FAT16   "

  .func WriteString
  WriteString:
    lodsb
    or al, al
    jz WriteString_done

    mov ah, 0xe
    mov bx, 9
    int 0x10
    jmp WriteString

  WriteString_done:
    retw
  .endfunc

.func Reboot
Reboot:
  lea si, rebootmsg
  call WriteString
  xor ax, ax
  int 0x16
  .byte 0xEA
  .word 0x0000
  .word 0xFFFF
.endfunc

start:

  cli
  mov iBootDrive, dl
  mov ax, cs
  mov ds, ax
  mov es, ax
  mov ss, ax
  mov sp, 0x7C00
  sti

  lea si, loadmsg
  call WriteString

  mov dl, iBootDrive
  xor ax, ax
  int 0x13
  jc bootFailure

  call reboot

bootFailure:
  lea si, diskerror
  call WriteString
  call Reboot

loadmsh: .asciz "boyOS is loading."
diskerror: .asciz "Disk error."
rebootmsg: .asciz "Press any key to reboot..."

.fill (510-(.-main)), 1, 0
BootMagic: .int 0xAA55
